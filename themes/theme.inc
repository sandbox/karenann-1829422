<?php

/**
 * @file
 *
 * An array of preprocessors to fill variables for templates and helper
 * functions to make theming easier.
 */

/**
 *
 */
function permassign_theme() {
  $path = drupal_get_path('module', 'permassign') . '/incs';
  return array(
    'permassign_permission_description' => array(
      'variables' => array('permission_item' => NULL, 'hide' => NULL),
    ),
    'permassign_manage_form' => array(
      'render element' => 'form',
    ),
  );
}

/**
 * Returns HTML for an individual permission description.
 *
 * @param $variables
 *   An associative array containing:
 *   - permission_item: An associative array representing the permission whose
 *     description is being themed. Useful keys include:
 *     - description: The text of the permission description.
 *     - warning: A security-related warning message about the permission (if
 *       there is one).
 *   - hide: A boolean indicating whether or not the permission description was
 *     requested to be hidden rather than shown.
 *
 * @ingroup themeable
 */
function theme_permassign_permission_description($variables) {
  if (!$variables['hide']) {
    $description = array();
    $permission_item = $variables['permission_item'];
    if (!empty($permission_item['description'])) {
      $description[] = $permission_item['description'];
    }
    if (!empty($permission_item['warning'])) {
      $description[] = '<em class="permission-warning">' . $permission_item['warning'] . '</em>';
    }
    if (!empty($description)) {
      return implode(' ', $description);
    }
  }
}

/**
 * Returns HTML for the administer permissions page.
 *
 * @param $variables
 *   An associative array containing:
 *   - form: A render element representing the form.
 *
 */
function theme_permassign_manage_form($variables) {

  // NOTE: Function copied and altered from modules/user/user.admin.inc, theme_user_admin_permissions

  $form = $variables['form'];

  $roles = user_roles();

  $rows = array();
  if (isset($form['permission'])) {
    foreach (element_children($form['permission']) as $key) {
      $row = array();
      // Module name
      if (is_numeric($key)) {
        $row[] = array('data' => drupal_render($form['permission'][$key]), 'class' => array('module'), 'id' => 'module-' . $form['permission'][$key]['#id'], 'colspan' => count($form['role_names']['#value']) + 1);
      }
      else {
        // Permission row.
        $row[] = array(
          'data' => drupal_render($form['permission'][$key]),
          'class' => array('permission'),
        );
        foreach (element_children($form['checkboxes']) as $rid) {
          $form['checkboxes'][$rid][$key]['#title'] = $roles[$rid] . ': ' . $form['permission'][$key]['#markup'];
          $form['checkboxes'][$rid][$key]['#title_display'] = 'invisible';
          if (isset($form['checkboxes'][$rid]['#references']))
            $form['checkboxes'][$rid][$key]['#suffix'] = '<div class="form-item-permission-checked" style="color: #ddd;">'
              . (in_array($key, $form['checkboxes'][$rid]['#references']) ? '+' : '-')
              . '</div>';
          $row[] = array('data' => drupal_render($form['checkboxes'][$rid][$key]), 'class' => array('checkbox'));
        }
      }
      $rows[] = $row;
    }
  }

  $header[] = (t('Permission'));
  if (isset($form['role_names'])) {
    foreach (element_children($form['role_names']) as $rid) {
      $header[] = array('data' => drupal_render($form['role_names'][$rid]), 'class' => array('checkbox'));
    }
  }

  $output  = theme('system_compact_link');
  $output .= drupal_render($form['actions']['autoselect']);
  $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'permissions')));
  $output .= drupal_render_children($form);

  return $output;
}

//EOF
