<?php

/**
 * @file
 * Functionality and helper functions for MODULEASSIGN administration.
 */

/**
 *
 */
function permassign_manage_form($form, &$form_state, $rid = NULL) {

  // Function copied and altered from modules/user/user.admin.inc, user_admin_permissions

  $path = current_path();

  switch (TRUE) {
    case (strpos($path, 'admin/people/permassign') === 0):
      $source = 'list';
      break;
    case (strpos($path, 'admin/config/people/permassign') === 0):
      $source = 'config';
      break;
    default:
      drupal_set_message(t('Could not produce page.'), 'error');
      return array();
  }

  if ($source == 'list') {
    global $user;
  }

  // Retrieve role names for columns.
  $role_names = user_roles();
  if (is_numeric($rid)) {
    $role_names = array($rid => $role_names[$rid]);
  }

  if ($source == 'list') {

    // Get highest role for this user
    if (empty($user->roles)) {
      drupal_set_message(t('There are no permissions to manage.'), 'warning');
      return array();
    }
    else {
      $role = end(array_keys($user->roles));
      $stop = FALSE;
      foreach ($role_names as $rid => $role_name) {
        if ($stop)
          unset($role_names[$rid]);
        else
          if ($rid == $role)
            $stop = TRUE;
      }
    }
  }

  // Fetch permissions for all roles or the one selected role.
  $role_permissions  = user_role_permissions($role_names);
  $permassign_status = permassign_role_permissions($role_names);

  if ($source == 'list') {
    $allowed = array();
    foreach ($user->roles as $rid => $role_name) {
      if (isset($permassign_status[$rid])) {
        $allowed = array_merge($allowed, $permassign_status[$rid]);
      }
    }
  }

  // Store $role_names for use when saving the data.
  $form['role_names'] = array(
    '#type' => 'value',
    '#value' => $role_names,
  );
  // Render role/permission overview:
  $options = array();
  $module_info = system_get_info('module');
  $hide_descriptions = system_admin_compact_mode();

  // Get a list of all the modules implementing a hook_permission() and sort by
  // display name.
  $modules = array();
  foreach (module_implements('permission') as $module) {
    $modules[$module] = $module_info[$module]['name'];
  }
  asort($modules);

  foreach ($modules as $module => $display_name) {

    if ($permissions = module_invoke($module, 'permission')) {

      if ($source == 'list') {
        $display = FALSE;
        foreach ($permissions as $perm => $perm_item) {
          if (isset($allowed[$perm])) {
            $display = TRUE;
            break;
          }
        }
        if (!$display) continue;
      }

      $form['permission'][] = array(
        '#markup' => $module_info[$module]['name'],
        '#id' => $module,
      );

      foreach ($permissions as $perm => $perm_item) {

        if ($source == 'list') {
          if (!isset($allowed[$perm])) continue;
        }

        // Fill in default values for the permission.
        $perm_item += array(
          'description' => '',
          'restrict access' => FALSE,
          'warning' => !empty($perm_item['restrict access']) ? t('Warning: Give to trusted roles only; this permission has security implications.') : '',
        );
        $options[$perm] = '';
        $form['permission'][$perm] = array(
          '#type' => 'item',
          '#markup' => $perm_item['title'],
          '#description' => filter_xss(theme('permassign_permission_description', array('permission_item' => $perm_item, 'hide' => $hide_descriptions))),
        );
        foreach ($role_names as $rid => $name) {
          // Builds arrays for checked boxes for each role
          if (isset($role_permissions[$rid][$perm])) {
            switch ($source) {
              case 'list':
                $status[$rid][] = $perm;
                break;
              case 'config':
                $references[$rid][] = $perm;
                break;
            }
          }
        }
        if ($source == 'config') {
          foreach ($role_names as $rid => $name) {
            // Builds arrays for checked boxes for each role
            if (isset($permassign_status[$rid][$perm])) {
              $status[$rid][] = $perm;
            }
          }
        }
      }
    }
  }

  if ($source == 'list') {
    if (empty($form['permission'])) {
      drupal_set_message(t('There are no permissions to manage.'), 'warning');
      return array();
    }
  }

  // Have to build checkboxes here after checkbox arrays are built
  foreach ($role_names as $rid => $name) {
    $form['checkboxes'][$rid] = array(
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => isset($status[$rid]) ? $status[$rid] : array(),
      '#attributes' => array('class' => array('rid-' . $rid)),
    );
    if ($source == 'config') {
      $form['checkboxes'][$rid]['#references'] = isset($references[$rid]) ? $references[$rid] : array();
    }
    $form['role_names'][$rid] = array('#markup' => check_plain($name), '#tree' => TRUE);
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save permissions'));

  // Add a button to autoselect permissions
  $form['actions']['autoselect'] = array(
    '#type' => 'submit', 
    '#value' => t('Sync Permissions'), 
    '#submit' => array('permassign_config_form_autoselect_submit'),
  );
  $form['actions']['autoselect']['#attributes']['class'][] = 'ibimage_right';
  
  // Add some controlling javascript
  $form['#attached']['js'][] = drupal_get_path('module', 'user') . '/user.permissions.js';

  switch ($source) {
    case 'list':
      $form['#submit'][] = 'permassign_set_form_submit';
      break;
    case 'config':
      $form['#submit'][] = 'permassign_config_form_submit';
      break;
  }

  return $form;
}

/**
 * Determine the permissions for one or more roles.
 *
 * @param $roles
 *   An array whose keys are the role IDs of interest, such as $user->roles.
 *
 * @return
 *   An array indexed by role ID. Each value is an array whose keys are the
 *   permission strings for the given role ID.
 */
function permassign_role_permissions($roles = array()) {

  // Function copied and altered from modules/user/user.admin.inc, user_role_permissions

  $cache = &drupal_static(__FUNCTION__, array());

  $permassign_status = $fetch = array();

  if ($roles) {
    foreach ($roles as $rid => $name) {
      if (isset($cache[$rid])) {
        $permassign_status[$rid] = $cache[$rid];
      }
      else {
        // Add this rid to the list of those needing to be fetched.
        $fetch[] = $rid;
        // Prepare in case no permissions are returned.
        $cache[$rid] = array();
      }
    }

    if ($fetch) {
      // Get from the database permissions that were not in the static variable.
      // Only role IDs with at least one permission assigned will return rows.
      $result = db_query("SELECT rid, permission FROM {permassign_permission} WHERE rid IN (:fetch)", array(':fetch' => $fetch));

      foreach ($result as $row) {
        $cache[$row->rid][$row->permission] = TRUE;
      }
      foreach ($fetch as $rid) {
        // For every rid, we know we at least assigned an empty array.
        $permassign_status[$rid] = $cache[$rid];
      }
    }
  }

  return $permassign_status;
}

/**
 * Save permissions selected on the administer permissions page.
 *
 * @see user_admin_permissions()
 */
function permassign_config_form_submit($form, &$form_state) {

  // Function copied and altered from modules/user/user.admin.inc, user_admin_permissions_submit

  foreach ($form_state['values']['role_names'] as $rid => $name) {
    permassign_role_change_permissions($rid, $form_state['values'][$rid]);
  }

  drupal_set_message(t('The changes have been saved.'));

  // Clear the cached pages and blocks.
  cache_clear_all();
}

/**
 * Submit routine for button to autoselect.
 */
function permassign_config_form_autoselect_submit() {
  
  // Get the default rid if different
  $rid = variable_get('permassign_default_role_rid', 0);
	
	// Complain! Somethings wrong!
	if ($rid < 1) {
		drupal_set_message(t('We could not locate the proper rid.'), 'error');
		return;
	}
  
  // Recheck all the default ones
  permassign_role_reset_permissions($rid);
  
  // Say hi
  drupal_set_message(t('The defaults for rid @rid have been reselected.', array('@rid' => $rid)));
  
}

/**
 * Save permissions selected on the administer permissions page.
 *
 * @see user_admin_permissions()
 */
function permassign_set_form_submit($form, &$form_state) {

  // Function copied and altered from modules/user/user.admin.inc, user_admin_permissions_submit

  foreach ($form_state['values']['role_names'] as $rid => $name) {
    user_role_change_permissions($rid, $form_state['values'][$rid]);
  }

  drupal_set_message(t('The changes have been saved.'));

  // Clear the cached pages and blocks.
  cache_clear_all();
}

/**
 * Change permissions for a user role.
 *
 * This function may be used to grant and revoke multiple permissions at once.
 * For example, when a form exposes checkboxes to configure permissions for a
 * role, the form submit handler may directly pass the submitted values for the
 * checkboxes form element to this function.
 *
 * @param $rid
 *   The ID of a user role to alter.
 * @param $permissions
 *   An associative array, where the key holds the permission name and the value
 *   determines whether to grant or revoke that permission. Any value that
 *   evaluates to TRUE will cause the permission to be granted. Any value that
 *   evaluates to FALSE will cause the permission to be revoked.
 *   @code
 *     array(
 *       'administer nodes' => 0,                // Revoke 'administer nodes'
 *       'administer blocks' => FALSE,           // Revoke 'administer blocks'
 *       'access user profiles' => 1,            // Grant 'access user profiles'
 *       'access content' => TRUE,               // Grant 'access content'
 *       'access comments' => 'access comments', // Grant 'access comments'
 *     )
 *   @endcode
 *   Existing permissions are not changed, unless specified in $permissions.
 *
 * @see user_role_grant_permissions()
 * @see user_role_revoke_permissions()
 */
function permassign_role_change_permissions($rid, array $permissions = array()) {

  // Function copied and altered from modules/user/user.admin.inc, user_role_change_permissions
  
  // Grant new permissions for the role.
  $grant = array_filter($permissions);
  if (!empty($grant)) {
    permassign_role_grant_permissions($rid, array_keys($grant));
  }
  // Revoke permissions for the role.
  $revoke = array_diff_assoc($permissions, $grant);
  if (!empty($revoke)) {
    permassign_role_revoke_permissions($rid, array_keys($revoke));
  }
}

/**
 * Restore default settings for the module permissions and assignments.
 *
 * @param $rid
 *   The ID of a user role to alter.
 * @param $permissions
 *   An associative array, where the key holds the permission name and the value
 *   determines whether to grant or revoke that permission. Any value that
 *   evaluates to TRUE will cause the permission to be granted. Any value that
 *   evaluates to FALSE will cause the permission to be revoked.
 *
 * @see permassign_role_change_permissions()
 * @see user_role_grant_permissions()
 * @see user_role_revoke_permissions()
 */
function permassign_role_reset_permissions($rid) {

  $all = user_permission_get_modules();
  
  $permissions = array();
  
  foreach ($all as $permission => $module) {
    if ($module == 'node') {
      $permissions[$permission] = $permission;
    }
  }

  permassign_role_change_permissions($rid, $permissions);
}

/**
 * Grant permissions to a user role.
 *
 * @param $rid
 *   The ID of a user role to alter.
 * @param $permissions
 *   A list of permission names to grant.
 *
 * @see user_role_change_permissions()
 * @see user_role_revoke_permissions()
 */
function permassign_role_grant_permissions($rid, array $permissions = array()) {

  // Function copied and altered from modules/user/user.admin.inc, user_role_grant_permissions

  $modules = user_permission_get_modules();
  // Grant new permissions for the role.
  foreach ($permissions as $name) {
    db_merge('permassign_permission')
      ->key(array(
        'rid' => $rid,
        'permission' => $name,
      ))
      ->fields(array(
        'module' => $modules[$name],
      ))
      ->execute();
  }

  // Clear the user access cache.
  drupal_static_reset('user_access');
  drupal_static_reset('permassign_role_permissions');
}

/**
 * Revoke permissions from a user role.
 *
 * @param $rid
 *   The ID of a user role to alter.
 * @param $permissions
 *   A list of permission names to revoke.
 *
 * @see user_role_change_permissions()
 * @see user_role_grant_permissions()
 */
function permassign_role_revoke_permissions($rid, array $permissions = array()) {

  // Function copied and altered from modules/user/user.admin.inc, user_role_revoke_permissions

  // Revoke permissions for the role.
  db_delete('permassign_permission')
    ->condition('rid', $rid)
    ->condition('permission', $permissions, 'IN')
    ->execute();

  // Clear the user access cache.
  drupal_static_reset('user_access');
  drupal_static_reset('permassign_role_permissions');
}

// EOF
